'use strict';

var amqp = require('amqplib');
var Promise = require('bluebird');

var debug = require('debug')('amqpevents:pool');

var pool = {
  connection: {},
  channel: {},
  exchange: {},
  queue: {}
};

exports.channel = function(url, opts) {
  debug('channel - url: %s, opts: %j', url, opts);
  debug('channel - key: %s', url);

  var cached = pool.channel[url];

  if (cached) {
    debug('channel - returning cached');
    return Promise.resolve(cached);
  }

  debug('channel - creating new channel');
  // wrap in bluebird promise - when and bluebird aren't compatible
  return new Promise(function(resolve, reject) {
    // create new connection and channel
    return amqp.connect(url, opts).then(function(conn) {
      pool.connection[url] = conn;
      conn.once('close', function() {
        delete pool.connection[url];
      });

      return conn;
    }).then(function(conn) {
      return conn.createChannel();
    }).then(function(ch) {
      pool.channel[url] = ch;
      ch.once('close', function() {
        delete pool.channel[url];
      });

      debug('channel - returning new channel');
      return ch;
    }).then(resolve, reject);
  });
};

exports.exchange = function(channel, opts) {
  debug('exchange - channel: %j, opts: %j', channel, opts);

  var key = opts.name + ':' + JSON.stringify(opts);
  debug('exchange - key: %s', key);

  var cached = pool.exchange[key];

  if (cached) {
    debug('exchange - returning cached');
    return Promise.resolve(cached);
  }

  debug('exchange - creating new exchange');
  return new Promise(function(resolve, reject) {
    var promise = channel.assertExchange(opts.name, opts.type, opts.opts);

    return promise.then(function() {
      pool.exchange[key] = opts.name;

      debug('exchange - returning new exchange');
      return opts.name;
    }).then(resolve, reject);
  });
};

exports.queue = function(opts) {
  debug('queue - opts.exchange: %s, opts.queue: %j', opts.exchange, opts.queue);

  var channel = opts.channel;
  var queueOpts = opts.queue;

  var key = queueOpts.name + ':' + opts.exchange + ':' + JSON.stringify(queueOpts.opts);
  debug('queue - key: %s', key);

  var cached = pool.queue[key];

  if (cached) {
    debug('queue - returning cached');
    return Promise.resolve(cached);
  }

  debug('queue - creating new queue');

  var promise = channel.assertQueue(queueOpts.name, queueOpts.opts);
  return promise.then(function(qok) {
    pool.queue[key] = qok.queue;

    channel.once('close', function() {
      delete pool.queue[key];
    });

    debug('queue - returning new queue');
    return qok.queue;
  });
};