# amqpevents

Event emitter using amqp as a data store and a topic exchange


## Usage

```js
var events = require('amqpevents');

// declare a publisher
var publisher = events
  .publisher('location')
  .connect('amqp://localhost')
  .event('location.locked')
  .event('location.lost');

// declare a subscriber
var subscriber = events
  .consume(publisher)
  .listener(console.info)
  .pattern('location.*')
  .subscribe();

// publish an event
publisher.publish('location.lost', { lat: 33.23, lng: 150.11 });
```


## [API Documentation](http://docs.knoxxnxt.com/amqpevents)
