/* globals describe, beforeEach, afterEach, it */

'use strict';

describe('#amqpevents', function() {
  var sinon = require('sinon');
  var expect = require('chai').expect;
  var Promise = require('bluebird');
  var amqp = require('amqplib');

  var events = require('./index');

  var publisher, subscriber, listener;

  beforeEach(function() {
    listener = sinon.spy();
  });

  beforeEach(function(done) {
    publisher = events.publisher('publisher')
      .connect('amqp://localhost')
      .event('a')
      .events(['a.a', 'b.a']);

    subscriber = events.subscriber('subscriber')
      .consume(publisher)
      .listener(listener)
      .pattern('a.*')
      .pattern('a')
      .subscribe({
        // for testing, remove queue after each test
        queue: { exclusive: true },
        // also dont need to ack
        consume: { noAck: true }
      });

    Promise.all([publisher._connectPromise, subscriber._connectPromise])
      .then(function() { done(); }).catch(done).done();
  });

  afterEach(function(done) {
    publisher.channel.close();
    publisher.channel.connection.close();

    publisher.channel.connection.once('close', done);
  });

  it('should throw an error on .publish() if event is not registered', function() {
    expect(function() {
      publisher.publish('aaa');
    }).to.throw(/unknown\sevent/i);
  });

  it.skip('should return a promsie when calling .publish()', function() {
    var rtn = publisher.publish('a');
    expect(rtn).to.have.property('state', 'pending');
  });

  it('should receive subscribed messages', function(done) {
    publisher.publish('a.a', { a: 'a' });
    publisher.publish('a', { a: { b: 'c' } });
    publisher.publish('b.a');

    setTimeout(function() {
      expect(listener.calledTwice).to.equal(true);
      expect(listener.firstCall.args).to.have.length(3);
      expect(listener.firstCall.args[0]).to.equal('a.a');
      expect(listener.firstCall.args[1]).to.eql({ a: 'a' });
      expect(listener.secondCall.args).to.have.length(3);
      expect(listener.secondCall.args[0]).to.equal('a');
      expect(listener.secondCall.args[1]).to.eql({ a: { b: 'c' } });

      done();
    }, 500);
  });

  it('should not receive other messages', function(done) {
    publisher.publish('b.a', { a: 'a' });

    setTimeout(function() {
      expect(listener.called).to.equal(false);
      done();
    }, 500);
  });

  it('should throw an error if an invalid event is declared for a publisher', function() {
    expect(function() {
      events.publisher('publisher').event('a.*');
    }).to.throw(/invalid\sevent/i);
  });

  it('should throw an error if an invalid pattern is declared for a subscriber', function() {
    expect(function() {
      var publisher = events.publisher('publisher').event('a.a');
      events.subscriber('subscriber')
        .consume(publisher)
        .pattern('a.**');
    }).to.throw(/invalid\spattern/i);
  });

  it('should throw an error if the pattern does not match a publisher event', function() {
    expect(function() {
      var publisher = events.publisher('publisher').event('a.a');
      events.subscriber('subscriber')
        .consume(publisher)
        .pattern('a.b.*');
    }).to.throw(/invalid\spattern/i);
  });

  it('should create a publisher with channel object', function(done) {
    amqp.connect('amqp://localhost').then(function(conn) {
      conn.createChannel().then(function(channel) {
        // magic happens here
        var listener = sinon.spy();

    var publisher = events.publisher('channel-publisher')
      .connect(channel)
      .event('a')
      .events(['a.a', 'b.a']);

    var subscriber = events.subscriber('channel-subscriber')
      .consume(publisher)
      .listener(listener)
      .pattern('a.*')
      .pattern('a')
      .subscribe({
        // for testing, remove queue after each test
        queue: { exclusive: true },
        // also dont need to ack
        consume: { noAck: true }
      });

        Promise.all([publisher._connectPromise, subscriber._connectPromise])
          .then(function() {
            publisher.publish('a', { a: { b: 'c' } });

            setTimeout(function() {
              expect(listener.calledOnce).to.equal(true);
              expect(listener.firstCall.args).to.have.length(3);
              expect(listener.firstCall.args[0]).to.equal('a');
              expect(listener.firstCall.args[1]).to.eql({ a: { b: 'c' } });

              done();
            }, 500);
          }).catch(done).done();
      }, done);
    });
  });
});