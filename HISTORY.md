# 0.0.2 (2 March 2015)

- Changed name from `amqp-events` to `amqpevents`


# 0.0.1 (23 Feb 2015)

- Initial commit