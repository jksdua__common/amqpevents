'use strict';

/**
  Events library using [amqplib](https://npmjs.com/package/amqplib)

  **Features:**

  - Create a `subscriber` to send messages
  - Create a `publisher` to receive messages
  - Wildcard support (as provided by amqp/rabbitmq)
  - Provides simple assertions to avoid common mistakes

  **Debugging:**

  Set the environment variable `DEBUG=amqpevents:*` to view verbose output.

  @module amqpevents
  @example
      var publisher = amqpEvents.publisher('publisher')
        .connect('amqp://localhost')
        .event('a')
        .events(['a.a', 'b.a']);

      var subscriber1 = amqpEvents.subscriber('subscriber1')
        .consume(publisher)
        .listener(console.log)
        .pattern('a.*')
        .subscribe();

      var subscriber2 = amqpEvents.subscriber('subscriber2')
        .consume(publisher)
        .listener(console.log)
        .patterns(['a.*', 'a'])
        .subscribe();
*/

var assert = require('assert');

var merge = require('lodash.merge');
var Promise = require('bluebird'); // jshint ignore:line
var stringify = require('json-stringify-safe');

var debugP = require('debug')('amqpevents:publisher');
var debugS = require('debug')('amqpevents:subscriber');

var pool = require('./pool');

var slice = Array.prototype.slice;

/**
  Publisher that sends messages

  @class Publisher
  @constructor
  @param {String} name Friendly name of the publisher - this is used as the **name of the exchange** so make sure this is unique

  @example
      var publisher = new amqpEvents.Publisher('master');
*/
function Publisher(name) {
  if (!(this instanceof Publisher)) {
    return new Publisher(name);
  }

  debugP('creating new publisher - name: %s', name);

  assert(name, 'Missing name');

  this.name = name;
  this._events = [];
}

var protoP = Publisher.prototype;

/**
  Name of the publisher

  Make sure this is unique as it is used as the name of the exchange on the server

  @property name
*/
protoP.name = null;

/**
  Internal array for holding events added using `.event()` and `.events()`

  @property _events
  @private
*/
protoP._events = null;

/**
  Promise created by `.connect()`

  @property _connectPromise
  @private
*/
protoP._connectPromise = null;

/**
  Add an event that the publisher emits.

  This does not impact the publisher. It is used to assert that the subscribers only subscribe to events that the publisher emits.

  @method event
  @param {String} event Event that the publisher emits
  @chainable

  @example
      publisher.event('some_event')
*/
protoP.event = function(event) {
  assert(this._events.indexOf(event) < 0, 'Duplicate event');
  assert(validEvent(event), 'Invalid event format');

  debugP('%s - adding event: %s', this.name, event);

  this._events.push(event);
  return this;
};

/**
  Adds multiple events that the publisher emits.

  This does not impact the publisher. It is used to assert that the subscribers only subscribe to events that the publisher emits.

  @method events
  @param {Array[String]} events Events that the publisher emits
  @chainable

  @example
      publisher.events(['a', 'b'])
*/
protoP.events = function(events) {
  debugP('%s - adding events: %j', this.name, events);

  events.forEach(this.event, this);
  return this;
};

/**
  Connect to an amqp instance

  Creates an amqp connection and channel. Since this is an asynchronous process, it sets the property `._connectPromise` to allow a user to watch for completion.

  @method connect
  @param {Object|String} [opts={url:'amqp://localhost',opts:{},exchange:{durable:true},channel:null}] If a string is passed, it is used as the url. Otherwise, pass an object with `url` and `opts` passed to `amqp.connect(opts.url, opts.opts)`. `exchange` is passed as options to `channel.assertExchange(<this.name>, opts.exchange)`. Also possible to pass a `channel` to use an existing channel.
  @chainable

  @example
      // no options
        // defaults to {url:'amqp://localhost',opts:{},exchange:{durable:true}}
      publisher.connect()

      // simple url
      publisher.connect('amqp://localhost')

      // options
      publisher.connect({
        exchange: { durable: false }
      })

      // channel object
      amqp.connect('amqp://localhost/test').then(function(conn) {
        conn.createChannel().then(function(channel) {
        publisher.connect({ channel: channel });
      }, console.error);
*/
protoP.connect = function(opts) {
  if ('string' === typeof opts) {
    opts = { url: opts };
  }

  // args passed to amqp.connect(url, opts)
  opts = this.opts = merge({
    url: 'amqp://localhost',
    opts: {},
    // make the exchange durable so it survives producer restarts
    exchange: { durable: true },
    channel: null
  }, opts);

  debugP('%s - connecting with opts: %j', this.name, opts);

  var self = this;

  // create channel or use existing one
  if (!opts.channel) {
    this._connectPromise = pool.channel(opts.url, opts.opts);
  } else {
    debugP('using existing channel passed in opts');

    this._connectPromise = Promise.resolve(opts.channel);
  }

  this._connectPromise = this._connectPromise.then(function(channel) {
    self.channel = channel;
    return channel;
  }).then(function() {
    return pool.exchange(self.channel, {
      name: self.name,
      type: 'topic',
      opts: opts.exchange
    });
  }).then(function(exchange) {
    self.exchange = exchange;
  }).then(function() {
    self.connected = true;
  });

  return this;
};

/**
  Publish an event

  @method publish
  @param {String} event Event to be published
  @param {Object} json JSON payload to be sent

  @example
      publisher.publish('a', { a: 'a' })
*/
protoP.publish = function(event, json) {
  debugP('%s - emitting event: %s, data: %j', this.name, event, json);

  assert(this.connected, 'Not connected to amqp server');
  assert(this._events.indexOf(event) > -1, 'Unknown event');

  return this.channel.publish(this.exchange, event, new Buffer(stringify(json || {})));
};

/**
  Subscriber for consuming publisher messages

  @class Subscriber
  @constructor
  @param {String} name Friendly name of the subscriber - this is used as the **name of the queue** so make sure this is unique

  @example
      var subscriber = new amqpEvents.Subscriber('slave');
*/
function Subscriber(name) {
  if (!(this instanceof Subscriber)) {
    return new Subscriber(name);
  }

  debugS('creating new subscriber - name: %s', name);

  this.publisher = null;
  this._listener = null;
  this._patterns = [];
  this.name = name;
}

var protoS = Subscriber.prototype;

/**
  Publisher consumed via `.consume()`

  @property publisher
  @type amqpEvents.Publisher
*/
protoS.publisher = null;

/**
  Listener that is called when an event is received

  @property _listener
  @type Function
  @private
*/
protoS._listener = null;

/**
  Patterns the subscriber is listening for

  @property _patterns
  @type Array
  @private
*/
protoS._patterns = null;

/**
  Subscriber name - used for creating the queue on the amqp server

  @property name
  @type String
*/
protoS.name = null;

/**
  Consume a publisher - a subscriber can only consume **ONE** publisher.

  @method consume
  @param {amqpEvents.Publisher} publisher Publisher to consume
  @chainable

  @example
      subscriber.consume(publisher);
*/
protoS.consume = function(publisher) {
  debugS('%s - consuming publisher: %s', this.name, publisher.name);

  assert(publisher instanceof Publisher, 'Missing/invalid publisher');
  this.publisher = publisher;

  return this;
};

/**
  Add a listener that is sent messages received by the client

  @method listener
  @param {Function} listener Listener function - format `listener(event, json, msg)`
  @param {Function} [errorHandler=console.error](err) If an error occurs due to payload parsing or synchronously thrown by the listener, it is caught and passed to the error handler
  @chainable

  @example
      function listener(event, json) {
        console.log('[%s] - %j', event, json);
      }

      function errorHandler(err) {
        console.error(err.stack);
      }

      subscriber.listener(listener, errorHandler);
*/
protoS.listener = function(listener, errorHandler) {
  var name = this.name;

  debugS('%s - adding listener with name: %s and errorHandler with name: %s', name, listener && listener.name, errorHandler && errorHandler.name);

  errorHandler = errorHandler || console.error;

  assert('function' === typeof listener, 'Invalid listener');
  assert('function' === typeof errorHandler, 'Invalid error handler');

  this._listener = function(msg) {
    debugS('%s - listener got msg', name);

    try {
      var event = msg.fields.routingKey;
      var json = JSON.parse(msg.content.toString());
      debugS('%s - msg event: %s, json: %j', name, event, json);

      listener(event, json, msg);
    // absorb error
    } catch(e) {
      debugS('%s - msg error: %s', name, e.stack || e.message || e);
      errorHandler(e);
    }
  };

  return this;
};

/**
  Subscribe to a pattern

  **Notes:**

  - By default, an error is thrown if the pattern does not match any subscriber events. To disable this feature, pass `opts` with `{ strict: false }`
  - The pattern may contain wildcard characters such as `#` and `*`

  @method pattern
  @param {String} pattern Wildcard pattern to subscribe to
  @param {Object} [opts={strict:true}] Pattern options
  @chainable

  @example
      subscriber
        .pattern('abc')
        .pattern('a.*')
        .pattern('#');
*/
protoS.pattern = function(pattern, opts) {
  opts = merge({ strict: true }, opts);
  debugS('%s - adding pattern: %s, opts: %j', this.name, pattern, opts);

  var warn = opts.strict ? assert.bind(null, false) : console.warn;

  if (!this.publisher) {
    warn('Call .consume() before .pattern() or .patterns(). This allows us to validate the pattern');
  }

  assert(!this.connected, 'Too late to add a pattern. Call .pattern() before .subscribe()');
  assert(this._patterns.indexOf(pattern) < 0, 'Duplicate pattern');
  assert(validBindingKey(pattern, this.publisher._events), 'Invalid pattern');

  this._patterns.push(pattern);

  return this;
};

/**
  Subscribe to multiple patterns

  **Notes:**

  - By default, an error is thrown if a pattern does not match any subscriber events. To disable this feature, pass `opts` with `{ strict: false }`
  - The patterns may contain wildcard characters such as `#` and `*`

  @method patterns
  @param {Array[String]} pattern Wildcard pattern to subscribe to
  @param {Object} [opts={strict:true}] Pattern options
  @chainable

  @example
      subscriber.patterns(['abc', 'a.*', '#']);
*/
protoS.patterns = function(patterns, opts) {
  debugS('%s - adding patterns: %j with opts: %j', this.name, patterns, opts);

  patterns.forEach(function(pattern) {
    this.pattern(pattern, opts);
  }, this);

  return this;
};

/**
  Commit subscription - call this last

  @method subscribe
  @param {Object} [opts] Subscription configuration
    @param {Object} [opts.queue={ exclusive: false, durable: true }] Queue options passed to `amqp.assertQueue`.
    @param {Object} [opts.consume={ noAck: false }] Options passed to `channel.consume`
  @chainable

  @example
*/
protoS.subscribe = function(opts) {
  opts = merge({
    queue: { exclusive: false, durable: true },
    consume: { noAck: false }
  }, opts);

  debugS('%s - subscribing with opts: %j', this.name, opts);

  // preconditions
  assert(this.publisher, 'Call .consume() before .subscribe()');
  assert(this._listener, 'Call .listener() before .subscribe()');
  assert(this._patterns.length, 'Add some patterns before .subscribe()');

  var self = this;
  var publisher = this.publisher;

  // if publisher is still connecting, we need to wait for it to finish
  var parentPromise = publisher._connectPromise.isFulfilled() ? Promise.resolve() : publisher._connectPromise;
  this._connectPromise = parentPromise.then(function() {
    var channel = publisher.channel;
    var exchange = publisher.exchange;

    return pool.queue({
      channel: channel,
      exchange: exchange,
      queue: { name: self.name, opts: opts.queue }
    });
  }).then(function(queue) {
    self.queue = queue;
  }).then(function(queue) {
    var channel = publisher.channel;
    var exchange = publisher.exchange;

    return Promise.all(self._patterns.map(function(pattern) {
      return channel.bindQueue(queue, exchange, pattern);
    })).then(function() {
      return queue;
    });
  }).then(function(queue) {
    var channel = publisher.channel;
    return channel.consume(queue, self._listener, opts.consume);
  }).then(function() {
    self.connected = true;
  });

  return this;
};

// http://regexr.com/3afla
var EVENT_REGEXP = /^(([a-z])(\.[a-z]+)*){1,255}$/;
function validEvent(str) { // jshint ignore:line
  return EVENT_REGEXP.test(str);
}

// http://regexr.com/3aflg
var WILDCARD_REGEXP = /^(([a-z]+)|([\#\*]))((\.(([a-z]+)|([\#\*])))?){1,255}$/;
function validBindingKey(str, events) { // jshint ignore:line
  var valid = WILDCARD_REGEXP.test(str);
  if (!valid) { return; }

  var bindingKeyRegexp = '^';
  slice.call(str).forEach(function(char) {
    if ('#' === char) {
      bindingKeyRegexp += '([a-z]+\.)*';
    } else if ('*' === char) {
      bindingKeyRegexp += '[a-z]+';
    } else if ('.' === char) {
      bindingKeyRegexp += '\.';
    } else {
      bindingKeyRegexp += char;
    }
  });
  bindingKeyRegexp += '$';

  var regexp = new RegExp(bindingKeyRegexp);
  return events.some(function(event) {
    return regexp.test(event);
  });
}

exports.publisher = exports.Publisher = Publisher;
exports.subscriber = exports.Subscriber = Subscriber;