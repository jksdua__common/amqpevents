YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "Publisher",
        "Subscriber"
    ],
    "modules": [
        "amqpevents"
    ],
    "allModules": [
        {
            "displayName": "amqpevents",
            "name": "amqpevents",
            "description": "Events library using [amqplib](https://npmjs.com/package/amqplib)\n\n**Features:**\n\n- Create a `subscriber` to send messages\n- Create a `publisher` to receive messages\n- Wildcard support (as provided by amqp/rabbitmq)\n- Provides simple assertions to avoid common mistakes\n\n**Debugging:**\n\nSet the environment variable `DEBUG=amqpevents:*` to view verbose output."
        }
    ]
} };
});